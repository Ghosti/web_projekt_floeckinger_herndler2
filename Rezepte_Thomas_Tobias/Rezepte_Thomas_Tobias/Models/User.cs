﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rezepte_Thomas_Tobias.Models
{
    public class User
    {
        public int ID { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public DateTime? Birthdate { get; set; }

        public Gender Gender { get; set; }

        public string EMail { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public UserRole UserRole { get; set; }

        public User() : this("", "", "", "", Gender.NotSpecified, null, -1, UserRole.NoUser, "")
        {
            
        }

        public User(string firstname, string lastname, string username, string email, Gender gender, DateTime? birthdate, int id, UserRole userrole, string password)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Username = username;
            this.EMail = email;
            this.Gender = gender;
            this.Birthdate = birthdate;
            this.ID = id;
            this.UserRole = userrole;
            this.Password = password;
        }

        public override string ToString()
        {
            return "Firstname: " + this.Firstname + Environment.NewLine
                + "Lastname: " + this.Lastname + Environment.NewLine
                + "Username: " + this.Username + Environment.NewLine
                + "EMail: " + this.EMail + Environment.NewLine
                + "Birthdate: " + this.Birthdate + Environment.NewLine
                + "UserRole: " + this.UserRole + Environment.NewLine
                + "Gender: " + this.Gender;
        }
    }
}