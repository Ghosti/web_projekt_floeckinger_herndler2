﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rezepte_Thomas_Tobias.Models
{
    public class UserPassword
    {
        public string User { get; set; }

        public string Password { get; set; }

        public UserPassword(string user, string password)
        {
            this.User = user;
            this.Password = password;
        }

        public UserPassword() : this("not-defined", "not-defined") { }
    }
}