﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rezepte_Thomas_Tobias.Models.DB
{
    public interface IRepositoryUsers
    {
        void Open();
        void Close();

        List<User> GetAllUsers();
        User GetUser(int id);
        User AuthenticateUser(string usernameoremail, string password);
    }
}
