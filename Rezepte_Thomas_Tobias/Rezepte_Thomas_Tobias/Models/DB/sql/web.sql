﻿drop Database if exists recipe;
drop Database recipe;
create Database recipe collate utf8_general_ci;

use recipe;

create table recipe(
    id int not null auto_increment,
    name varchar(100) null,
    description varchar(250) null,
    username varchar(100) not null, 
    Personen int not null,
    
    constraint id_PK primary key(id),
    constraint foreign key (username) references Users(username) 
    
)engine=InnoDB;

create table ingredientspool(
    id int not null auto_increment,
    name varchar(100) null,
    
    constraint id_PK primary key(id)
)engine=InnoDB;

create table ingredients(
    id int not null auto_increment,
	recipe_id int not null,
	ingredientspool_id int not null, 
    Menge double null,
    Einheit varchar(50) null, 
    
    constraint id_PK primary key(id),
    constraint ingredientspool_id_FK foreign key(ingredientspool_id) references ingredientspool(id),
	constraint recipe_id_FK foreign key(recipe_id) references recipe(id)

)engine=InnoDB; 

create table comments(
	id int not null auto_increment,
    commentsrecipe_id int not null,
    comment varchar(500) null,
    userid int not null,
    
    constraint id_PK primary key(id),
	constraint commentsrecipe_id_FK foreign key(commentsrecipe_id) references recipe(id),
    constraint userid_FK foreign key(userid) references users(id) 
    
)engine=InnoDB;

insert into recipe values(null,"Kuchen","Schokoladenkuchen mit Himbergeschmack","Ghosti", 4);
insert into recipe values(null,"Donut","Zuckerdonut mit Nüssen","Ghosti", 2);
insert into ingredientspool values (null, "Schokolade");
insert into ingredientspool values (null, "Mehl");
insert into ingredientspool values (null, "Zucker");
insert into ingredientspool values (null, "Nüsse");

insert into ingredients values (null, 1, 1, 500, "Gramm"); 
insert into ingredients values (null, 1, 2, 300, "Gramm"); 
insert into ingredients values (null, 2, 2, 250, "Gramm");
insert into ingredients values (null, 2, 3, 50, "Gramm");
insert into ingredients values (null, 2, 4, 100, "Gramm");

insert into comments values (null, 1, "Test1", 1);
insert into comments values(null, 2, "Donut", 1); 

select * from comments;
select * from recipe
join ingredients on (ingredients.recipe_id = recipe.id)
join ingredientspool on (ingredients.ingredientspool_id = ingredientspool.id)
join comments on (comments.commentsrecipe_id = recipe.id);


create table Users
(
	id int not null auto_increment,
    firstname varchar(100) null,
    lastname varchar(100) not null,
    birthdate date null,
    gender int null,
    email varchar(100) not null,
    username varchar(100) not null unique,
    passwrd varchar(40) not null,
    isAdmin tinyint not null default 0,
    
    constraint id_PK primary key (id)
)engine = InnoDB;

insert into Users values(null, "Thomas", "Herndler", '2000-09-07', 0, 'thomasherndler@gmail.com', 'Herndschlberndschl', sha1("yo"), 1);
insert into Users values(null, "Tobias", "Flöckinger", '2001-01-21', 0, 'toflöckinger@tsn.at', 'Ghosti', sha1("ghost"), 1);

select * from users;

Alter Table Users add constraint UserNameIsUnique unique(username);  
alter table Recipe add constraint UserNameReferenz foreign key (username) references Users(username); 

select * from comments; 

DELETE FROM comments WHERE comment LIKE '%Strikner%';

Select comment, users.username, userid, comments.id from comments join recipe on (recipe.id = comments.commentsrecipe_id) join users on (users.id = comments.userid) where recipe.id = 2; 