﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace Rezepte_Thomas_Tobias.Models.DB
{
    public class RepositoryUsers : IRepositoryUsers
    {
        private string _connectionString = "Server=localhost; Database=recipe; UID=root;";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }
            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
            {
                this._connection.Close();
            }
        }

        public List<User> GetAllUsers()
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            List<User> allusers = new List<User>();

            MySqlCommand cmd = this._connection.CreateCommand();
            cmd.CommandText = "Select * from users";

            try
            {
                using(MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        allusers.Add(
                            new User
                            {
                                ID = Convert.ToInt32(reader["id"]),
                                Firstname = Convert.ToString(reader["firstname"]),
                                Lastname = Convert.ToString(reader["lastname"]),
                                Birthdate = reader.IsDBNull(3) ? DateTime.MinValue : Convert.ToDateTime(reader["birthdate"]),
                                Gender = (Gender)Convert.ToInt32(reader["gender"]),
                                EMail = Convert.ToString(reader["email"]),
                                Username = Convert.ToString(reader["username"]),
                                UserRole = Convert.ToBoolean(reader["isAdmin"]) ? UserRole.Admin : UserRole.RegisteredUser
                            }
                        );
                    }
                }

                return allusers.Count == 0 ? null : allusers;
            }
            catch(Exception)
            {
                throw;
            }
        }

        public User GetUser(int id)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            MySqlCommand cmd = this._connection.CreateCommand();
            cmd.CommandText = "Select * from users where ID = @userid";
            cmd.Parameters.AddWithValue("userid", id);

            try
            {
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.HasRows)
                    {
                        reader.Read();
                        return new User
                            {
                                ID = Convert.ToInt32(reader["id"]),
                                Firstname = Convert.ToString(reader["firstname"]),
                                Lastname = Convert.ToString(reader["lastname"]),
                                Birthdate = reader.IsDBNull(3) ? DateTime.MinValue : Convert.ToDateTime(reader["birthdate"]),
                                Gender = (Gender)Convert.ToInt32(reader["gender"]),
                                EMail = Convert.ToString(reader["email"]),
                                Username = Convert.ToString(reader["username"]),
                                UserRole = Convert.ToBoolean(reader["isAdmin"]) ? UserRole.Admin : UserRole.RegisteredUser
                            };
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return null;
        }

        public User AuthenticateUser(string usernameoremail, string password)
        {
            try
            {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM users WHERE ((username = @usernameOrEMail) AND (passwrd = sha1(@password)) OR ((email = @usernameOrEMail) AND (passwrd = sha1(@password))))";
                cmdAuthenticate.Parameters.AddWithValue("usernameOrEMail", usernameoremail);
                cmdAuthenticate.Parameters.AddWithValue("password", password);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return new User
                        {
                            ID = Convert.ToInt32(reader["id"]),
                            Firstname = Convert.ToString(reader["firstname"]),
                            Lastname = Convert.ToString(reader["lastname"]),
                            Birthdate = reader.IsDBNull(3) ? DateTime.MinValue : Convert.ToDateTime(reader["birthdate"]),
                            Gender = (Gender)Convert.ToInt32(reader["gender"]),
                            EMail = Convert.ToString(reader["email"]),
                            Username = Convert.ToString(reader["username"]),
                            UserRole = Convert.ToBoolean(reader["isAdmin"]) ? UserRole.Admin : UserRole.RegisteredUser
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Insert(User persontoinsert)
        {
            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return false;
            }

            if (persontoinsert == null)
            {
                return false;
            }

            try
            {
                // Command (Befehl) erzeugen
                MySqlCommand cmdInsert = this._connection.CreateCommand();

                cmdInsert.CommandText = "insert into users values (null, @firstname, @lastname, @birthdate, @gender, @email, @username, sha1(@pwd), @isAdmin)";
                cmdInsert.Parameters.AddWithValue("firstname", persontoinsert.Firstname);
                cmdInsert.Parameters.AddWithValue("lastname", persontoinsert.Lastname);
                cmdInsert.Parameters.AddWithValue("birthdate", persontoinsert.Birthdate);
                cmdInsert.Parameters.AddWithValue("gender", persontoinsert.Gender);
                cmdInsert.Parameters.AddWithValue("email", persontoinsert.EMail);
                cmdInsert.Parameters.AddWithValue("username", persontoinsert.Username);
                cmdInsert.Parameters.AddWithValue("pwd", persontoinsert.Password);
                cmdInsert.Parameters.AddWithValue("isAdmin", persontoinsert.UserRole == UserRole.Admin);
                return cmdInsert.ExecuteNonQuery() == 1;
            }
            catch (Exception)
            {
                // die erzeugte Exception wird erneut ausgelöst
                throw;
            }
        }
    }
}