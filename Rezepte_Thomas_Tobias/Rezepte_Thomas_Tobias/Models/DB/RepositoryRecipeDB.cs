﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Rezepte_Thomas_Tobias.Models.ViewModels;

namespace Rezepte_Thomas_Tobias.Models.DB
{
    public class RepositoryRecipeDB : IRepositoryRecipe
    {
        private string _connectionString = "Server=localhost; Database=recipe; UID=root;";
        private MySqlConnection _connection;

        public void Open()
        {
            if (this._connection == null)
            {
                this._connection = new MySqlConnection(this._connectionString);
            }
            if (this._connection.State != ConnectionState.Open)
            {
                this._connection.Open();
            }
        }

        public void Close()
        {
            if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
            {
                this._connection.Close();
            }
        }

        public List<Ingredients> GetAllIngredients(Recipe recipe)
        {
            List<Ingredients> ingredients = new List<Ingredients>();

            try
            {
                MySqlCommand cmdAllIngredients = this._connection.CreateCommand();
                cmdAllIngredients.CommandText = "Select * From ingredients join ingredientspool on (ingredientspool.id = ingredients.ingredientspool_id) where recipe_id = " + recipe.Id;
                using (MySqlDataReader reader = cmdAllIngredients.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ingredients.Add(new Ingredients()
                        {
                            Name = Convert.ToString(reader["name"]), 
                            Menge = Convert.ToDouble(reader["menge"]),
                            Einheit = Convert.ToString(reader["einheit"]),
                        });
                    }
                }
                return ingredients.Count == 0 ? null : ingredients;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Comments> GetAllComments (Recipe recipe)
        {
            List<Comments> comments = new List<Comments>();

            try
            {
                MySqlCommand cmdAllComments = this._connection.CreateCommand();
                cmdAllComments.CommandText = "Select comment, users.username, userid, comments.id from comments join recipe on (recipe.id = comments.commentsrecipe_id) join users on (users.id = comments.userid) where recipe.id = " + recipe.Id; 
                using (MySqlDataReader reader = cmdAllComments.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        comments.Add(new Comments()
                        {
                            Comment = Convert.ToString(reader["comment"]),
                            UserID = Convert.ToInt32(reader["userid"]), 
                            Username = Convert.ToString(reader["username"]),
                            Id = Convert.ToInt32(reader["id"]),
                        }); 
                    }
                }
                return comments.Count == 0 ? null : comments; 
            }
            catch(Exception)
            {
                throw; 
            }
        }

        public List<Recipe> GetAllRecipe()
        {
            List<Recipe> recipe = new List<Recipe>();

            try
            {
                MySqlCommand cmdAllRecipe = this._connection.CreateCommand();
                cmdAllRecipe.CommandText = "SELECT * FROM recipe";

                using (MySqlDataReader reader = cmdAllRecipe.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        recipe.Add(new Recipe()
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Description = Convert.ToString(reader["description"]),
                            Username = Convert.ToString(reader["username"]),
                            CountPeople = Convert.ToInt32(reader["Personen"]), 
                        });
                    }
                }
                return recipe.Count == 0 ? null : recipe;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public Recipe GetOneRecipe(int id)
        {
            Recipe recipe = new Recipe();

            try
            {
                MySqlCommand cmdAllRecipe = this._connection.CreateCommand();
                cmdAllRecipe.CommandText = "SELECT * FROM recipe where id = @ID";
                cmdAllRecipe.Parameters.AddWithValue("ID", id); 

                using (MySqlDataReader reader = cmdAllRecipe.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        recipe = new Recipe()
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Description = Convert.ToString(reader["description"]),
                            Username = Convert.ToString(reader["username"]),
                            CountPeople = Convert.ToInt32(reader["Personen"]),
                        };
                        return recipe;
                    }
                }
                return null;

            }
            catch (Exception)
            {
                throw;
            }

        }

        public void SaveNewComment(Comments comment)
        {
            try
            {
                MySqlCommand cmdInsert = this._connection.CreateCommand();

                cmdInsert.CommandText = "insert into comments values(null, @ID, @comment, @userid)";
                cmdInsert.Parameters.AddWithValue("ID", comment.RecipeID);
                cmdInsert.Parameters.AddWithValue("comment", comment.Comment);
                cmdInsert.Parameters.AddWithValue("userid", comment.UserID);

                cmdInsert.ExecuteNonQuery(); 

            }
            catch (Exception)
            {
                throw;
            }

        }

        public Comments GetOneComment (int commentID)
        {
            try
            {
                Comments comment = new Comments(); 
                MySqlCommand cmdOneComment = this._connection.CreateCommand();

                cmdOneComment.CommandText = "select * from comments where id = " + commentID;
                using (MySqlDataReader reader = cmdOneComment.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        comment = new Comments()
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Comment = Convert.ToString(reader["comment"]), 
                        };
                        return comment;
                    }
                }
                return null;

            }
            catch (Exception)
            {
                throw; 
            }
        }

        public void DeleteComment(int commentID)
        {
            try
            {
                MySqlCommand cmdDelete = this._connection.CreateCommand();

                cmdDelete.CommandText = "Delete From comments where comments.id = " + commentID;
                cmdDelete.ExecuteNonQuery(); 
            }
            catch (Exception)
            {
                throw; 
            }
        }
        public void SaveEditComment (string comment, int commentID)
        {
            try
            {
                MySqlCommand cmdSaveComment = this._connection.CreateCommand();

                cmdSaveComment.CommandText = "UPDATE comments SET comment = '" + comment + "' WHERE id = " + commentID;
                cmdSaveComment.ExecuteNonQuery(); 
            }
            catch(Exception)
            {
                throw; 
            }
        }

    }
}