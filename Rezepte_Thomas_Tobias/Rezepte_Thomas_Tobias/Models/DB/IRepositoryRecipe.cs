﻿using System;
using System.Collections.Generic;
using Rezepte_Thomas_Tobias.Models.ViewModels; 

namespace Rezepte_Thomas_Tobias.Models.DB
{
    public interface IRepositoryRecipe
    {
        void Open();
        void Close();

        //Recipe GetRecipe(int idToFind);
        List<Recipe> GetAllRecipe();
        List<Ingredients> GetAllIngredients(Recipe recipe);
        Recipe GetOneRecipe(int id);
        void SaveNewComment(Comments comment);
        List<Comments> GetAllComments(Recipe recipe);
        void DeleteComment(int commentID);
        Comments GetOneComment(int commentID);
        void SaveEditComment(string comment, int commentID); 
        //void SaveNewRecipe(); 
    }
}
