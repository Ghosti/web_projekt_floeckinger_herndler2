﻿using System;
namespace Rezepte_Thomas_Tobias.Models
{
    public class Ingredients
    {
        public string Name { get; set; }
        public double Menge { get; set; }
        public string Einheit { get; set; }

        public Ingredients() : this("", 0.0, "") { }
        public Ingredients(string name, double menge, string einheit)
        {
            this.Menge = menge;
            this.Name = name;
            this.Einheit = einheit; 
        }
    }
}
