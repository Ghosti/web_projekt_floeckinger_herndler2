﻿using System;
using System.Collections.Generic;
namespace Rezepte_Thomas_Tobias.Models.ViewModels
{
    public class OneRecipeViewModel
    {
        public string Recipe { get; set; }
        public List<Ingredients> Ingredients { get; set; }
        public int ID { get; set; }
        public int CountPeople { get; set; }
        public List<Comments> Comments { get; set; }
        public Comments NewComment { get; set; }
        public string Description { get; set; }


        public OneRecipeViewModel() : this ("", null, 0, 0, null, null, "") { }
        public OneRecipeViewModel(string recipe, List<Ingredients> ingredients, int id, int personen, 
            List<Comments> comments, Comments newComment, string description)
        {
            this.Recipe = recipe;
            this.Ingredients = ingredients;
            this.ID = id;
            this.CountPeople = personen;
            this.Comments = comments;
            this.NewComment = newComment;
            this.Description = description; 
        }

        public override string ToString()
        {
            string IDString = this.ID + ".jpg";
            return IDString;
        }
    }
}
