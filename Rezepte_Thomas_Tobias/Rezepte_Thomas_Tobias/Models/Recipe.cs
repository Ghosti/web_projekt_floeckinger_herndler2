﻿using System;
using System.Collections.Generic;
namespace Rezepte_Thomas_Tobias.Models
{
    public class Recipe
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { if (_id >= 0)
                {
                    _id = value;
                } 
            }
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public int CountPeople { get; set; }

        public Recipe () : this (0, "", "", "", 0) { }
        public Recipe(int id, string name, string description, string username, int countPeople)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Username = username;
            this.CountPeople = countPeople; 
        }

        public string User()
        {
            string UserN = Username;
            return UserN; 
        }
    }
}
