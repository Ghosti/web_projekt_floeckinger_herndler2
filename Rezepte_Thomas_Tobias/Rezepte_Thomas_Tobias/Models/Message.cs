﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rezepte_Thomas_Tobias.Models
{
    public class Message
    {
        public string Header { get; set; }
        public string AdditionalHeader { get; set; }
        public string MessageText { get; set; }
        public string Solution { get; set; }

        public Message() : this("", "", "", "") { }
        public Message(string header, string message):this(header, "", message, "") { }
        public Message(string header, string adheader, string messagetext, string solution)
        {
            this.Header = header;
            this.AdditionalHeader = adheader;
            this.MessageText = messagetext;
            this.Solution = solution;
        }
    }
}