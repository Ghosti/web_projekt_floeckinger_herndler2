﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rezepte_Thomas_Tobias.Models
{
    public enum UserRole
    {
        Admin,
        RegisteredUser,
        NoUser
    }
}