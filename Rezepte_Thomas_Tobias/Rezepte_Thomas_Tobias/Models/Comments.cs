﻿using System;
namespace Rezepte_Thomas_Tobias.Models
{
    public class Comments
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public int UserID { get; set; }
        public int RecipeID { get; set; }
        public string Username { get; set; }

        public Comments () : this (0, "", 0, 0, "") { }
        public Comments (int id, string comment, int userID, int recipeID, string username)
        {
            this.Id = id;
            this.Comment = comment;
            this.UserID = userID;
            this.RecipeID = recipeID;
            this.Username = username; 
        }
    }
}
