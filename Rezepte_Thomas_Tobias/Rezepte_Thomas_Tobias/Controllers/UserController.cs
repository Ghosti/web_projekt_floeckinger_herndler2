﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rezepte_Thomas_Tobias.Models.DB;
using Rezepte_Thomas_Tobias.Models;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace Rezepte_Thomas_Tobias.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Repository()
        {
            IRepositoryUsers repusers = new RepositoryUsers();

            try
            {
                repusers.Open();
                return View(repusers.GetAllUsers());
            }
            catch
            {
                return View("Message", new Message("Server sind Offline", "Aus unerwarteten Gründen sind die Server leider momentan nicht erreichbar. Wir bitten um Ihr Verständnis."));
            }
            finally
            {
                repusers.Close();
            }
        }

        public ActionResult OneUser(int id = 1)
        {
            IRepositoryUsers repusers = new RepositoryUsers();
            
            try
            {
                repusers.Open();
                return View(repusers.GetUser(id));
            }
            catch
            {
                return View("Message", new Message("Server sind Offline", "Aus unerwarteten Gründen sind die Server leider momentan nicht erreichbar. Wir bitten um Ihr Verständnis."));
            }
            finally
            {
                repusers.Close();
            }
        }

        [HttpGet]
        public ActionResult Authenticate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authenticate(UserPassword up)
        {
            if(up != null)
            {
                ValidateLogin(up);

                if(ModelState.IsValid)
                {
                    return View("Message", new Message("Login erfolgreich", "Sie haben sich erfolgreich eingeloggt."));
                }
            }

            return View("Authenticate");
        }

        private void ValidateLogin(UserPassword up)
        {
            RepositoryUsers repUsers = new RepositoryUsers();

            try
            {
                repUsers.Open();
                User u = repUsers.AuthenticateUser(up.User, up.Password);
                if (u == null) // Person mit username und password konnte nicht gefunden werden und somit ist das Login fehlgeschlagen
                {
                    ModelState.AddModelError("Login-Failed", "Beim Login ist ein Fehler entstanden. Überprüfen Sie, ob der Username und das Passwort korrekt sind.");
                }
                else
                {
                    this.Session["LoggedUserObject"] = u;
                    this.Session["LoggedUser"] = u.ID;
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Server-Error", "Es entstand ein Fehler beim Zugriff auf den Server.");
            }
            finally
            {
                repUsers.Close();
            }
        }

        [HttpGet] //HTTPGET aufruf bei anklicken eines links
        public ActionResult Registration()
        {
            User u = new User();
            return View(u);
        }

        [HttpPost] //HTTPPOST aufruf wenn formular mit methode post abgesendet wird
        public ActionResult Registration(User personDataFromForm)
        {
            //parameter überprüfen
            if (personDataFromForm != null)
            {
                //formulardaten überprüfen
                ValidationForRegistrationForm(personDataFromForm);
                //zB in DB speichern
                //hier meldung ausgeben
                if (ModelState.IsValid)
                {
                    RepositoryUsers repUsers = new RepositoryUsers();
                    
                    try
                    {
                        repUsers.Open();
                        if (repUsers.Insert(personDataFromForm))
                        {
                            return View("Message", new Message("Registierung", "Sie wurden erfolgreich registriert."));
                        }
                        else
                        {
                            return View("Message", new Message("Registierung", "Sie konnten nicht erfolgreich registriert werden."));
                        }
                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "Es gibt zur Zeit Probleme mit der Datenbank."));
                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Allgemeiner Fehler", "Es ist ein allgemeiner Fehler aufgetreten."));
                    }
                    finally
                    {
                        repUsers.Close();
                    }
                }
                else
                {
                    return View(personDataFromForm);
                }
            }
            else
            {
                return RedirectToAction("Registration");
            }
        }

        private void ValidationForRegistrationForm(User person)
        {
            if ((person.Firstname != null) && (person.Firstname.Trim().Length < 3))
            {
                ModelState.AddModelError("Firstname", "Der Vorname muss mind. 3 Zeichen lang sein!");
            }
            if ((person.Lastname == null) || (person.Lastname.Trim().Length < 3))
            {
                ModelState.AddModelError("Lastname", "Der Nachname ist ein Pflichtfeld und muss mind. 3 Zeichen lang sein!");
            }
            if (person.Birthdate >= DateTime.Today)
            {
                ModelState.AddModelError("BirthDate", "Das angegebene Alter ist ungültig!");
            }
            if (person.EMail == null)
            {
                ModelState.AddModelError("EMail", "E-Mail Adresse ist ein Pflichtfeld!");
            }
            if ((person.Username != null) && (person.Username.Trim().Length < 5))
            {
                ModelState.AddModelError("Username", "Der Benutzername muss mind. 5 Zeichen lang sein.");
            }
            if ((person.Password == null) || (person.Password.Length < 8) ||
                (person.Password.IndexOfAny(new[] { '!', '$', '%', '/', '§' }) == -1))
            {
                ModelState.AddModelError("Password", "Das Passwort muss mind. 8 Zeichen lang sein und ein Sonderzeichen enthalten!");
            }
        }
    }
}