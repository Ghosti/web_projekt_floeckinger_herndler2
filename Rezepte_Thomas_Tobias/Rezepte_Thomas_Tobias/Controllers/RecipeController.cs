﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rezepte_Thomas_Tobias.Models.DB;
using Rezepte_Thomas_Tobias.Models;
using Rezepte_Thomas_Tobias.Models.ViewModels;
using System.Diagnostics;

namespace Rezepte_Thomas_Tobias.Controllers
{
    public class RecipeController : Controller
    {
        IRepositoryRecipe repRecipe;
        List<Recipe> rpList = new List<Recipe>();
        List<Ingredients> igList = new List<Ingredients>(); 

        public ActionResult Index()
        {
            return View ();
        }

        [HttpGet]
        public ActionResult ShowAllRecipes()
        {
            repRecipe = new RepositoryRecipeDB(); 
            try
            {
                repRecipe.Open();
                rpList = repRecipe.GetAllRecipe();
            }
            catch (Exception)
            {
                return View("Message", new Message());
            }
            finally
            {
                repRecipe.Close();
            }
            return View(rpList);
        }

        [HttpGet]
        public ActionResult OneRecipe(int id = 1)
        {
            try
            {
                Recipe oneRecipe;
                repRecipe = new RepositoryRecipeDB();

                repRecipe.Open();
                oneRecipe = repRecipe.GetOneRecipe(id);
                if (oneRecipe != null)
                {
                    OneRecipeViewModel oneRecipeVM = new OneRecipeViewModel();
                    oneRecipeVM.Recipe = oneRecipe.Description;
                    oneRecipeVM.Ingredients = repRecipe.GetAllIngredients(oneRecipe);
                    oneRecipeVM.ID = oneRecipe.Id;
                    oneRecipeVM.CountPeople = oneRecipe.CountPeople;
                    oneRecipeVM.Comments = repRecipe.GetAllComments(oneRecipe);
                    Session["RecipeID"] = oneRecipeVM.ID;

                    return View(oneRecipeVM);
                }
                else { return null; }
            }
            catch (Exception)
            {
                throw; 
            }
            finally
            {
                repRecipe.Close(); 
            }
        }
        [HttpPost]
        public ActionResult OneRecipe(Comments newComment) 
        {
            try
            {
                if (newComment.Comment != null)
                {
                    repRecipe = new RepositoryRecipeDB();
                    if (Session["loggedUser"] == null)
                    {
                        return RedirectToAction("../User/Authenticate");
                    }
                    if (Session["RecipeID"] == null)
                    {
                        return RedirectToAction("OneRecipe");
                    }
                    newComment.RecipeID = (int)Session["RecipeID"];
                    newComment.UserID = (int)Session["loggedUser"];
                    repRecipe.Open();
                    repRecipe.SaveNewComment(newComment);
                    return RedirectToAction("OneRecipe");

                }
                else
                {
                    return RedirectToAction("OneRecipe");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                repRecipe.Close(); 
            }
        }
        [HttpPost]
        public ActionResult DeleteComment(int commentID)
        {
            try
            {
                repRecipe = new RepositoryRecipeDB();
                repRecipe.Open();
                repRecipe.DeleteComment(commentID);
                int RecipeID = (int)Session["RecipeID"];
                return RedirectToAction("../Recipe/OneRecipe/" + RecipeID);
            }
            catch (Exception)
            {
                throw; 
            }
            finally
            {
                repRecipe.Close(); 
            }

        }
        [HttpPost]
        public ActionResult EditComment(int commentID)
        {
            try
            {
                Recipe oneRecipe; 
                repRecipe = new RepositoryRecipeDB();
                repRecipe.Open();
                oneRecipe = repRecipe.GetOneRecipe((int)Session["RecipeID"]); 
                if (oneRecipe != null)
                {
                    OneRecipeViewModel oneRecipeVM = new OneRecipeViewModel();
                    oneRecipeVM.Recipe = oneRecipe.Description;
                    oneRecipeVM.Ingredients = repRecipe.GetAllIngredients(oneRecipe);
                    oneRecipeVM.ID = oneRecipe.Id;
                    oneRecipeVM.CountPeople = oneRecipe.CountPeople;
                    oneRecipeVM.NewComment = repRecipe.GetOneComment(commentID);
                    return View(oneRecipeVM);
                }
                else { return null; }

            }
            catch(Exception)
            {
                throw;
            }
            finally
            {
                repRecipe.Close(); 
            }
        }
        [HttpPost]
        public ActionResult SaveEditComment(int commentID, string comment)
        {
            try
            {
                if (comment != null)
                {
                    repRecipe = new RepositoryRecipeDB();
                    if (Session["loggedUser"] == null)
                    {
                        return RedirectToAction("../User/Authenticate");
                    }
                    if (Session["RecipeID"] == null)
                    {
                        return RedirectToAction("OneRecipe");
                    }
                    repRecipe.Open();
                    repRecipe.SaveEditComment(comment, commentID);
                    int RecipeID = (int)Session["RecipeID"];
                    return RedirectToAction("../Recipe/OneRecipe/" + RecipeID);

                }
                else
                {
                    return RedirectToAction("OneRecipe");
                }
            }
            catch(Exception)
            {
                throw;
            }
            finally
            {
                repRecipe.Close();
            }
        }
        [HttpGet]
        public ActionResult SaveNewRecipe()
        {
            return View(); 
        }
    }
}
